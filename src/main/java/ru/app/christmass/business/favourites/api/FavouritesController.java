package ru.app.christmass.business.favourites.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.app.christmass.business.favourites.FavouriteEvent;
import ru.app.christmass.business.favourites.api.dto.AddFavouriteEventRequest;
import ru.app.christmass.business.favourites.service.FavouritesService;

@RestController
@RequestMapping(value = "/api/favourites", produces = MediaType.APPLICATION_JSON_VALUE)
public class FavouritesController {

    @Autowired
    FavouritesService service;

    @PostMapping("/add_event")
    public FavouriteEvent addEvent(AddFavouriteEventRequest request) {
        return service.add(request.getEventId(), request.getUserId());
    }

}

