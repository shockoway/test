package ru.app.christmass.business.favourites.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.app.christmass.business.event.service.EventService;
import ru.app.christmass.business.favourites.FavouriteEvent;
import ru.app.christmass.business.favourites.repository.FavouritesRepository;
import ru.app.christmass.business.user.service.UserService;
import ru.app.christmass.utils.BadRequestException;

@Service
public class FavouritesService {

    @Autowired
    UserService userService;

    @Autowired
    EventService eventService;

    @Autowired
    FavouritesRepository repository;

    public FavouriteEvent add(int eventId, int userId) {

        eventService.findEvent(eventId)
                .orElseThrow(() -> new BadRequestException("Событие не найдено"));
        userService.findUser(userId)
                .orElseThrow(() -> new BadRequestException("Пользователь не найден"));

        FavouriteEvent favouriteEvent = new FavouriteEvent(0, userId, eventId);
        return repository.save(favouriteEvent);
    }
}
