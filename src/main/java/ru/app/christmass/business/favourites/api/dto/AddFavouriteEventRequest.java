package ru.app.christmass.business.favourites.api.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Positive;

@Getter
@Setter
public class AddFavouriteEventRequest {

    @Positive
    int eventId;

    @Positive
    int userId;

}
