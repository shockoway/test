package ru.app.christmass.business.favourites.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.app.christmass.business.favourites.FavouriteEvent;

@Repository
public interface FavouritesRepository extends JpaRepository<FavouriteEvent, Integer> {
}
