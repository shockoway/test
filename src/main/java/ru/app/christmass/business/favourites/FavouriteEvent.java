package ru.app.christmass.business.favourites;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class FavouriteEvent {

    @Id
    @GeneratedValue
    int id;

    @Column(nullable = false)
    int userId;

    @Column(nullable = false)
    int eventId;

}
