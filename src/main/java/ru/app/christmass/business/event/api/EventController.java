package ru.app.christmass.business.event.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.app.christmass.business.event.Event;
import ru.app.christmass.business.event.api.dto.CheckEventsResponse;
import ru.app.christmass.business.event.api.dto.UpdateEventRequest;
import ru.app.christmass.business.event.service.EventService;

import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@RestController
@RequestMapping(value = "/api/events", produces = MediaType.APPLICATION_JSON_VALUE)
public class EventController {

    @Autowired
    EventService service;

    @PostMapping("/update")
    public Event updateEvent(@RequestBody @Validated UpdateEventRequest request) {
        Event event = new Event();
        event.setId(request.getId());
        event.setName(request.getName());
        event.setDescription(request.getDescription());
        event.setAddress(request.getAddress());
        event.setDate(request.getTime());
        return service.updateEvent(event);
    }

    @GetMapping("/get_events")
    public List<Event> getEvents(@RequestParam @DateTimeFormat(iso = DATE_TIME) LocalDateTime from,
                                 @RequestParam @DateTimeFormat(iso = DATE_TIME) LocalDateTime to) {
        return service.getEvents(from, to);
    }

    @GetMapping("/check_events")
    public CheckEventsResponse checkEvents(@RequestParam @Positive int id,
                                           @RequestParam @DateTimeFormat(iso = DATE_TIME) LocalDateTime time) {
        var event = service.checkEvent(id, time).orElse(null);
        return new CheckEventsResponse(event);
    }



}
