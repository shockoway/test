package ru.app.christmass.business.event;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table
public class Event {

    @Id
    @GeneratedValue
    int id;

    @Column(nullable = false)
    String name;

    @Column(nullable = false)
    String description;

    @Column(nullable = false)
    String address;

    @Column(nullable = false)
    LocalDateTime date;

    @Column(nullable = false)
    LocalDateTime lastUpdate;

}
