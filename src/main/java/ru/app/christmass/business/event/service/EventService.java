package ru.app.christmass.business.event.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.app.christmass.business.event.Event;
import ru.app.christmass.business.event.repository.EventRepository;
import ru.app.christmass.utils.BadRequestException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class EventService {

    @Autowired
    EventRepository repository;

    public List<Event> getEvents(LocalDateTime from, LocalDateTime to) {
        return repository.findByDateBetween(from, to);
    }

    public Optional<Event> checkEvent(int id, LocalDateTime checkDate) {
        Event event = repository.findById(id)
                .orElseThrow(() -> new BadRequestException("Событие не найдено"));

        if (checkDate.isBefore(event.getLastUpdate())) {
            return Optional.of(event);
        } else {
            return Optional.empty();
        }
    }

    public Event updateEvent(Event event) {
        event.setLastUpdate(LocalDateTime.now());
        return repository.save(event);
    }

    public Optional<Event> findEvent(int eventId) {
        return repository.findById(eventId);
    }
}

