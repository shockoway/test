package ru.app.christmass.business.event.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.lang.Nullable;
import ru.app.christmass.business.event.Event;

@AllArgsConstructor
@Getter
public class CheckEventsResponse {
    @Nullable
    Event event;
}
