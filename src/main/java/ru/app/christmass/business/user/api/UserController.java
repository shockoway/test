package ru.app.christmass.business.user.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.app.christmass.business.user.User;
import ru.app.christmass.business.user.api.dto.AddUserRequest;
import ru.app.christmass.business.user.api.dto.AddUserResponse;
import ru.app.christmass.business.user.api.dto.CheckUserRequest;
import ru.app.christmass.business.user.api.dto.CheckUserResponse;
import ru.app.christmass.business.user.service.UserService;

@RestController
@RequestMapping(value = "/api/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    @Autowired
    UserService service;

    @PostMapping("/add_user")
    public AddUserResponse addUser(@RequestBody @Validated AddUserRequest request) {
        var user = service.addUser(request.getUsername(), request.getPassword());
        return new AddUserResponse(user.getId());
    }

    @PostMapping("/check_user")
    public CheckUserResponse checkUser(@RequestBody @Validated CheckUserRequest request) {
        var userId = service.checkUser(request.getUsername(), request.getPassword())
                .map(User::getId)
                .orElse(null);
        return new CheckUserResponse(userId);
    }

}
