package ru.app.christmass.business.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.app.christmass.business.user.User;
import ru.app.christmass.business.user.repository.UserRepository;
import ru.app.christmass.utils.BadRequestException;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository repository;

    public User addUser(String username, String password) {
        var existingUser = repository.findUserByUsername(username);
        if (existingUser.isPresent()) {
            throw new BadRequestException("Пользователь с таким логином уже существует");
        }
        // FIXME допустимо ли хранить пароли в открытом виде?
        User user = new User(0, username, password);
        return repository.save(user);
    }

    public Optional<User> checkUser(String username, String password) {
        Optional<User> optional = repository.findUserByUsername(username);
        if (optional.isPresent()) {
            User user = optional.get();
            if (password.equals(user.getPassword())) {
                return optional;
            }
        }
        return Optional.empty();
    }

    public Optional<User> findUser(int userId) {
        return repository.findById(userId);
    }
}

