package ru.app.christmass.business.user.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CheckUserResponse {
    Integer userId;
}
