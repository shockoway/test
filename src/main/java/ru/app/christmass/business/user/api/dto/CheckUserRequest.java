package ru.app.christmass.business.user.api.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class CheckUserRequest {

    @NotBlank
    String username;

    @NotBlank
    String password;

}
