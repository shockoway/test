package ru.app.christmass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChristmassApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChristmassApplication.class, args);
	}

}
